package com.android.impulsivebuyers

import android.content.Context

/**
 * Created by spoor on 3/12/2018.
 */
interface BasePresenter {

    fun subscribe()
    fun unsubscribe()
    fun setPresenter(context:Context)

}