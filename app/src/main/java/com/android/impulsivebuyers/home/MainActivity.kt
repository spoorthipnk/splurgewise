package com.android.impulsivebuyers.home

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.Toast
import com.android.impulsivebuyers.R
import com.android.impulsivebuyers.data.Item
import com.android.impulsivebuyers.data.SplurgeWiseDatabase
import com.android.impulsivebuyers.dbworker.DbWorkerThread
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MainActivity : AppCompatActivity() {

    private var splurgeWiseDatabase:SplurgeWiseDatabase? = null
    private lateinit var mDbWorkerThread: DbWorkerThread
    private val mUiHandler = Handler()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mDbWorkerThread = DbWorkerThread("dbWorkerThread")
        mDbWorkerThread.start()

        splurgeWiseDatabase = SplurgeWiseDatabase.getInstance(this)

        val item1 = Item(0,"abcchvhgvgh",1)
        val item2 = Item(0,"fdght",1)

        Single.fromCallable{
            splurgeWiseDatabase?.itemDao()?.insert(item1)
            splurgeWiseDatabase?.itemDao()?.insert(item2)
        }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe()

        splurgeWiseDatabase?.itemDao()?.getItems()
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe { listOfItems -> process(listOfItems) }



    }

    fun process(itemList:List<Item> ) {

        for(item in itemList){
            Log.i("roomdb",item.itemName)
        }

    }
}
