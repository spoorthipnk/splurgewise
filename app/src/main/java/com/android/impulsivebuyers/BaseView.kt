package com.android.impulsivebuyers

/**
 * Created by spoor on 3/12/2018.
 */
interface BaseView<T> {
    fun setPresenter(presenter:T)
}