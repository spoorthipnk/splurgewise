package com.android.impulsivebuyers.additem

import com.android.impulsivebuyers.BaseModel
import com.android.impulsivebuyers.BasePresenter
import com.android.impulsivebuyers.BaseView

/**
 * Created by spoor on 3/12/2018.
 */
interface AddItemContract {

    interface View:BaseView<Presenter>
    interface Presenter:BasePresenter
    interface Model:BaseModel<Presenter>


}