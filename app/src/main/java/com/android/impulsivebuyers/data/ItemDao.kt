package com.android.impulsivebuyers.data

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import io.reactivex.Flowable

@Dao
interface ItemDao {

    @Query("Select * from Items")
    fun getItems():Flowable<List<Item>>

    @Insert(onConflict = REPLACE)
    fun insert(item:Item)

}