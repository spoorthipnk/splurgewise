package com.android.impulsivebuyers.data

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context

@Database(entities = arrayOf(Item::class),version = 1)
abstract class SplurgeWiseDatabase:RoomDatabase() {

    abstract fun itemDao(): ItemDao

    companion object {

        private var INSTANCE:SplurgeWiseDatabase? = null

        fun getInstance(context: Context) : SplurgeWiseDatabase?{
            if(INSTANCE == null){
                synchronized(SplurgeWiseDatabase::class){
                    INSTANCE = Room.databaseBuilder(
                            context.applicationContext,
                            SplurgeWiseDatabase::class.java,
                            "splurgewise.db")
                            .build()
                }
            }

            return INSTANCE
        }

        fun destroyInstance(){
            INSTANCE = null
        }
    }

}