package com.android.impulsivebuyers.data

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "items")
data class Item (@PrimaryKey(autoGenerate = true) var id:Long?, var itemName:String? = null, var priority:Int = 0 )
